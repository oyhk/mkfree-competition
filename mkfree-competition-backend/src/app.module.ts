import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { CompetitionModule } from './competition/competition.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mkfree-competition.db',
      synchronize: true,
      logging: true,
      autoLoadEntities: true,
    }),
    UserModule,
    CompetitionModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
