import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    origin: '*',
    methods: ['GET', 'HEAD', 'PUT', 'PATCH', 'POST', 'DELETE'],
    credentials: true,
    allowedHeaders: '*',
    maxAge: 3600,
    optionsSuccessStatus: 204,
    preflightContinue: false,
  });
  await app.listen(5002);
}
bootstrap();
