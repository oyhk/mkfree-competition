import {
  Body,
  Controller, Delete,
  Get, Inject,
  Post,
  Put,
  Query, Req,
  Res,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Response } from 'express';
import { ARC } from '../common/api-result';
import { Competition } from './competition.entity';
import { CompetitionLog } from './competition-log.entity';
import { User } from '../user/user.entity';
import { CompetitionLogDto } from './competition-log.dto';
import * as lodash from 'lodash';

@Controller()
export class CompetitionLogController {
  constructor(
    @InjectRepository(Competition)
    private competitionRepository: Repository<Competition>,
    @InjectRepository(CompetitionLog)
    private competitionLogRepository: Repository<CompetitionLog>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {

  }


  /**
   * 比赛每一局计分 保存
   * @param dto
   * @param req
   * @param res
   */
  @Post('/api/competition-log/save')
  async save(@Body() dto: CompetitionLogDto, @Req() req, @Res() res: Response) {
    console.log('dto', dto);

    if (!dto.competitionId) {
      return res.json(ARC['2']('CompetitionLog.id cannot be blank'));
    }
    if (dto.bomb === undefined) {
      return res.json(ARC['2']('CompetitionLog.bomb cannot be blank'));
    }
    if (dto.landlordWin === undefined) {
      return res.json(ARC['2']('CompetitionLog.landlordWin cannot be blank'));
    }
    if (!dto.landlordUserId) {
      return res.json(ARC['2']('CompetitionLog.landlordUserId cannot be blank'));
    }

    const competition = await this.competitionRepository.findOne(dto.competitionId);
    if (!competition) {
      return res.json(ARC['3']('competition record does not exist'));
    }
    competition.current = competition.current + 1;

    const landlordUserId = competition.userList.filter(value => value.id === dto.landlordUserId).map(value => (value.id))[0];
    if (!landlordUserId) {
      return res.json(ARC['3']('illegal landlord user in the competition'));
    }
    const landlordUser = await this.userRepository.findOne(landlordUserId);
    const farmerUserIdList = competition.userList.filter(value => value.id !== dto.landlordUserId).map(value => value.id);

    // 地主计分
    const landlordUserCompetitionLog = new CompetitionLog();
    landlordUserCompetitionLog.competitionId = competition.id;
    landlordUserCompetitionLog.userId = landlordUser.id;
    landlordUserCompetitionLog.userName = landlordUser.name;
    // 地主胜，计分这里使用等比数列公式 求 An = a1 * q^(n-1)，这里默认q等于2
    const a1 = competition.score * 2; // 地主底分需要乘以2
    const q = 2;
    const n = dto.bomb + 1;
    const An = a1 * Math.pow(q, n - 1);
    const landlordScore = dto.landlordWin ? An : -An;
    landlordUserCompetitionLog.score = landlordScore;
    landlordUserCompetitionLog.current = competition.current;
    landlordUserCompetitionLog.isLandlord = true;
    await this.competitionLogRepository.save(landlordUserCompetitionLog);

    // 农民计分
    for (const farmerUserId of farmerUserIdList) {
      const landlordUser = await this.userRepository.findOne(farmerUserId);
      const farmerUserCompetitionLog = new CompetitionLog();
      farmerUserCompetitionLog.competitionId = competition.id;
      farmerUserCompetitionLog.userId = landlordUser.id;
      farmerUserCompetitionLog.userName = landlordUser.name;
      farmerUserCompetitionLog.score = -landlordScore / 2;
      farmerUserCompetitionLog.current = competition.current;
      farmerUserCompetitionLog.isLandlord = false;
      await this.competitionLogRepository.save(farmerUserCompetitionLog);
    }
    //
    await this.competitionRepository.update(competition.id, competition);

    return res.json(ARC['1']());
  }

  @Delete('/api/competition-log/delete')
  async delete(@Body() dto: CompetitionLogDto, @Req() req, @Res() res: Response) {
    console.log('dto', dto);

    if (!dto.competitionId) {
      return res.json(ARC['2']('CompetitionLog.competitionId cannot be blank'));
    }

    if (!dto.current) {
      return res.json(ARC['2']('CompetitionLog.current cannot be blank'));
    }
    // 删除日志
    await this.competitionLogRepository.delete({ current: dto.current, competitionId: dto.competitionId });

    // 重新编排日志局数
    const competitionLogList = await this.competitionLogRepository.find({ competitionId: dto.competitionId });
    const competitionLogListGroupByCurrent = lodash.groupBy(competitionLogList, 'current');
    let current = 0;
    for (const key of Object.keys(competitionLogListGroupByCurrent)) {
      current++;
      for (const competitionLog of competitionLogListGroupByCurrent[key]) {
        if (competitionLog.current !== current) {
          competitionLog.current = current;
          await this.competitionLogRepository.update(competitionLog.id, competitionLog);
        }
      }
    }

    // 更新比赛场次
    await this.competitionRepository.update(dto.competitionId, { current: current });

    res.json(ARC['1']());
  }
}
