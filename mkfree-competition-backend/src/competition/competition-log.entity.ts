import {
  Entity,
  Column,
} from 'typeorm';
import { BaseEntity } from '../common/base.entity';

@Entity()
export class CompetitionLog extends BaseEntity {

  static entityName = 'CompetitionLog';

  @Column({ comment: '比赛 id' })
  competitionId: number;
  @Column({ comment: '用户 名称' })
  userName: string;
  @Column({ comment: '用户 id' })
  userId: number;
  @Column({ comment: '分值' })
  score: number;
  @Column({ comment: '当前为第几场比赛' })
  current: number;
  @Column({ default: false, comment: '当前为第几场比赛' })
  isLandlord: boolean;

}