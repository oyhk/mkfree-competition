import {
  Body,
  Controller, Delete,
  Get, Inject,
  Post,
  Put,
  Query, Req,
  Res,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Page } from '../common/page';
import { Response } from 'express';
import { ARC, AR } from '../common/api-result';
import { Competition, CompetitionType } from './competition.entity';
import { CompetitionDto } from './competition.dto';
import { User } from '../user/user.entity';
import { CompetitionLog } from './competition-log.entity';
import * as lodash from 'lodash';

@Controller()
export class CompetitionController {
  constructor(
    @InjectRepository(Competition)
    private competitionRepository: Repository<Competition>,
    @InjectRepository(CompetitionLog)
    private competitionLogRepository: Repository<CompetitionLog>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {

  }

  @Get('/api/competition/page')
  async page(@Query() userDto: CompetitionDto, @Res() res: Response) {
    const ar = new AR();
    const page = new Page<Competition>();
    await this.competitionRepository.findAndCount({ order: { id: 'DESC' } }).then(value => {
      page.data = value[0];
      page.total = value[1];
    });
    page.pageNo = userDto.pageNo;
    page.pageSize = userDto.pageSize;
    // 分页总数，总记录数 / 页条数，当存在小数点，使用了 Math.ceil 直接网上取整数
    page.totalPage = Math.ceil(page.total / page.pageSize);
    ar.result = page;
    return res.json(ar);
  }

  @Get('/api/competition/info')
  async info(@Query() dto: CompetitionDto, @Res() res: Response) {

    if (!dto.id) {
      return res.json(ARC['2']('Competition.id cannot be blank'));
    }

    const competition = await this.competitionRepository.findOne(dto.id) as CompetitionDto;
    if (!competition) {
      return res.json(ARC['3']('competition record does not exist'));
    }
    competition.userList = competition.userList.sort((a, b) => b.id - a.id);
    for (const user of competition.userList) {
      const dbUser = await this.userRepository.findOne(user.id);
      user.name = dbUser.name;
      user.score = 0;
    }

    const competitionLogList = await this.competitionLogRepository.find({
      where: { competitionId: dto.id },
      order: { id: 'DESC' },
    });

    if (competitionLogList.length > 0) {
      const groupByCurrentCompetitionLog = lodash.groupBy(competitionLogList, 'current');
      const groupByCurrentCompetitionLogSort = lodash.toPairs(groupByCurrentCompetitionLog)?.sort((pre, cur) => parseInt(cur[0]) - parseInt(pre[0]))?.map(value => ({
        current: value[0],
        logList: value[1].sort((a, b) => b.userId - a.userId),
      }));

      const groupByUserIdCompetitionLog = lodash.groupBy(competitionLogList, 'userId');

      for (const user of competition.userList) {
        user.score = groupByUserIdCompetitionLog[user.id].map(value => value.score).reduce((pre, cur) => pre + cur);
      }
      competition.competitionLogList = groupByCurrentCompetitionLogSort;

    }
    return res.json(ARC['1'](competition));
  }

  /**
   * 比赛 保存
   * @param dto
   * @param req
   * @param res
   */
  @Post('/api/competition/save')
  async save(@Body() dto: CompetitionDto, @Req() req, @Res() res: Response) {
    if (!dto.userList) {
      return res.json(ARC['2']('competition.userIdList cannot be blank'));
    }
    // 目前默认为斗地主
    dto.type = CompetitionType.landlord.code;
    // 当前第几局，默认为0
    dto.current = 0;
    const competition = await this.competitionRepository.save(dto);
    return res.json(ARC['1'](competition));
  }

  /**
   * 比赛 删除
   * @param dto
   * @param req
   * @param res
   */
  @Delete('/api/competition/delete')
  async delete(@Body() dto: CompetitionDto, @Req() req, @Res() res: Response) {

    if (!dto.id) {
      return res.json(ARC['2']('competition.id cannot be blank'));
    }

    const competition = await this.competitionRepository.findOne(dto.id);
    if (!competition) {
      return res.json(ARC['3'](`${Competition.entityName} params: ${JSON.stringify({ id: dto.id })}, recode be blank`));
    }


    await this.competitionLogRepository.delete({ competitionId: competition.id });

    await this.competitionRepository.delete(competition.id);

    return res.json(ARC['1']());
  }


}
