import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Competition } from './competition.entity';
import { CompetitionController } from './competition.controller';
import { CompetitionLog } from './competition-log.entity';
import { User } from '../user/user.entity';
import { CompetitionLogController } from './competition-log.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Competition,CompetitionLog,User])],
  providers: [],
  controllers: [CompetitionController,CompetitionLogController],
})
export class CompetitionModule {

}
