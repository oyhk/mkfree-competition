import { Competition } from './competition.entity';
import { User } from '../user/user.entity';
import { CompetitionLog } from './competition-log.entity';
import { Column } from 'typeorm';

/**
 * 比赛 dto
 */
export class CompetitionLogDto extends CompetitionLog {
  /**
   * 多少炸
   */
  bomb: number;
  /**
   * 地主
   */
  landlordUserId:number;
  /**
   * 地主胜或败
   */
  landlordWin:boolean;


}