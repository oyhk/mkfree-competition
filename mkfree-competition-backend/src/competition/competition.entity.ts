import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { BaseEntity } from '../common/base.entity';
import { User } from '../user/user.entity';

@Entity()
export class Competition extends BaseEntity {
  static entityName = 'Competition';

  @Column({ comment: '底分' })
  score: number;
  @Column({ comment: '比赛类型，引用 CompetitionType' })
  type: string;
  @Column({ comment: '比赛用户列表', type: 'simple-json' })
  userList: { id: number, name: string, score: number }[];
  @Column({ default: 0, comment: '当前为第几场比赛' })
  current: number;
}

export const CompetitionType = {
  landlord: {
    code: 'Landlord',
    desc: '斗地主',
  },
  bigTwo: {
    code: 'BigTwo',
    desc: '锄大地',
  },
};