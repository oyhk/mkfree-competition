import { Competition } from './competition.entity';
import { CompetitionLog } from './competition-log.entity';

/**
 * 比赛 dto
 */
export class CompetitionDto extends Competition {

  competitionLogList: { current: string, logList: CompetitionLog[] }[];

}