/**
 * ApiResult 用于API请求异常处理提示
 */
export class AR {
  code?: number;
  desc?: string;
  result?: any;

  constructor(code?: number, desc?: string, result?: any) {
    this.code = code;
    this.desc = desc;
    this.result = result;
  }

}

export const ARC = {
  0: new AR(0, 'fail'),
  1: (result?) => new AR(1, 'success', result),
  2: (descEx?: string) => new AR(2, `params remind: ${descEx}`),
  3: (descEx?: string) => new AR(3, `warm remind: ${descEx}`),
};