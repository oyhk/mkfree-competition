import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import { BaseEntity } from '../common/base.entity';

@Entity()
export class User extends BaseEntity {
  static entityName = 'User';

  @Column()
  name: string;
}
