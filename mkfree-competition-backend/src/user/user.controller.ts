import {
  Body,
  ClassSerializerInterceptor,
  Controller, Delete,
  Get, Inject,
  Post,
  Put,
  Query, Req,
  Res,
  UseInterceptors,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Page } from '../common/page';
import { Response } from 'express';
import { User } from './user.entity';
import { UserDto } from './user.dto';
import { AR, ARC } from '../common/api-result';
import { CompetitionDto } from '../competition/competition.dto';
import { Competition } from '../competition/competition.entity';

@Controller()
export class UserController {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {
  }

  @Get('/api/user/list')
  async list(@Query() userDto: UserDto, @Res() res: Response) {
    const userList = await this.userRepository.find({ order: { id: 'DESC' } });
    return res.json(ARC['1'](userList));
  }

  @Get('/api/user/page')
  async page(@Query() userDto: UserDto, @Res() res: Response) {
    const ar = new AR();
    const page = new Page<User>();


    await this.userRepository.findAndCount({ order: { id: 'DESC' } }).then(value => {
      page.data = value[0];
      page.total = value[1];
    });
    page.pageNo = userDto.pageNo;
    page.pageSize = userDto.pageSize;
    // 分页总数，总记录数 / 页条数，当存在小数点，使用了 Math.ceil 直接网上取整数
    page.totalPage = Math.ceil(page.total / page.pageSize);
    ar.result = page;
    return res.json(ar);
  }


  @Post('/api/user/save')
  async save(@Body() dto: UserDto, @Req() req, @Res() res: Response) {
    console.log('dto', dto);
    if (!dto.name) {
      return res.json(ARC['2']('user.name cannot be blank'));
    }
    let user = await this.userRepository.findOne({ name: dto.name });
    if (user) {
      return res.json(ARC['3']('User already exists'));
    }
    user = new User();
    user.name = dto.name;
    await this.userRepository.save(user);
    return res.json(ARC['1']());
  }

  /**
   * 用户 删除
   * @param dto
   * @param req
   * @param res
   */
  @Delete('/api/user/delete')
  async delete(@Body() dto: UserDto, @Req() req, @Res() res: Response) {

    if (!dto.id) {
      return res.json(ARC['2']('user.id cannot be blank'));
    }

    const user = await this.userRepository.findOne(dto.id);
    if (!user) {
      return res.json(ARC['3'](`${Competition.entityName} params: ${JSON.stringify({ id: dto.id })}, recode be blank`));
    }
    await this.userRepository.delete(user.id);
    return res.json(ARC['1']());
  }

}
