// 开发
// const gateway ='http://192.168.8.108:5002/';

// 生产
const gateway = 'http://mkfreecompetition.tech.yiwoaikeji.com/';

export const HttpMethod = {
    get: 'get',
    post: 'post',
    put: 'put',
    delete: 'delete',
};

const parseParams = (data: any) => {
    try {
        const tempArr = [];
        for (const i in data) {
            const key = encodeURIComponent(i);
            const value = encodeURIComponent(data[i]);
            tempArr.push(key + '=' + value);
        }
        return tempArr.join('&');
    } catch (err) {
        return '';
    }
};

export const ApiRoutes = {

    userPage: (searchParams?: any) => ({
        url: `${gateway}api/user/page?${parseParams(searchParams)}`,
        method: HttpMethod.get,
    }),
    userList: (searchParams?: any) => ({
        url: `${gateway}api/user/list?${parseParams(searchParams)}`,
        method: HttpMethod.get,
    }),
    userSave: (payload?: any) => ({
        url: `${gateway}api/user/save`,
        method: HttpMethod.post,
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
    }),
    userDelete: (payload?: any) => ({
        url: `${gateway}api/user/delete`,
        method: HttpMethod.delete,
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
    }),
    userUpdate: (payload?: any) => ({
        url: `${gateway}api/user/update`,
        method: HttpMethod.put,
        data: payload,
    }),

    competitionPage: (searchParams?: any) => {
        return ({
            url: `${gateway}api/competition/page?${parseParams(searchParams)}`,
            method: HttpMethod.get,
        })
    },
    competitionInfo: (searchParams?: any) => ({
        url: `${gateway}api/competition/info?${parseParams(searchParams)}`,
        method: HttpMethod.get,
    }),
    competitionSave: (payload?: any) => ({
        url: `${gateway}api/competition/save`,
        method: HttpMethod.post,
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
    }),

    competitionDelete: (payload?: any) => ({
        url: `${gateway}api/competition/delete`,
        method: HttpMethod.delete,
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
    }),

    competitionLogSave: (payload?: any) => ({
        url: `${gateway}api/competition-log/save`,
        method: HttpMethod.post,
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
    }),

    competitionLogDelete: (payload?: any) => ({
        url: `${gateway}api/competition-log/delete`,
        method: HttpMethod.delete,
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
    }),
};

