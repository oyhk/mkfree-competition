import { Text, TextInput, View} from "react-native";
import React, {useState} from "react";
import UserDto from "./user.dto";
import {ApiResult} from "../service/ApiResult";
import {useRequest} from "ahooks";
import {ApiRoutes} from "../service/ApiRoutes";
import {CommonActions} from '@react-navigation/native';
import {Button} from "react-native-paper";

export default (props: any) => {


    const [user] = useState<UserDto>({});

    const projectSaveUseRequest = useRequest<ApiResult<any>>(
        (payload) => {
            return ApiRoutes.userSave(payload);
        },
        {
            onSuccess: (ar, params) => {
                if (ar.code === 1) {
                    props.navigation.dispatch(
                        CommonActions.navigate({
                            name: 'Users',
                            params: {
                                isGoBack: true
                            },
                        })
                    );
                } else {
                    alert(JSON.stringify(ar));
                }

            },
            manual: true,
            refreshOnWindowFocus: false,
        },
    );

    return (
        <View style={{backgroundColor: '#ffffff'}}>
            <View style={{flexDirection: 'row', padding: 20}}>
                <Text style={{flex: 1, height: 40, lineHeight: 40}}>Name：</Text>
                <TextInput onChangeText={(value) => user.name = value}
                           style={{flex: 3, height: 40, borderColor: 'gray', borderWidth: 1}}/>
            </View>
            <View style={{margin: 20}}>
                <Button
                    mode={'contained'}
                        onPress={() => {
                            projectSaveUseRequest.run(user);
                        }}
                >Submit</Button>
            </View>
        </View>
    )
}