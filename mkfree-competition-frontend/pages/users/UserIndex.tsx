import {
    View,
    Text,
    StyleSheet,
    FlatList,
    StatusBar,
    TouchableOpacity,
    TouchableHighlight,
    BackHandler, Alert
} from "react-native";
import React, {useState} from "react";

import {useRequest} from 'ahooks';
import {ApiResult} from "../service/ApiResult";
import {PageResult} from "../service/PageResult";
import {ApiRoutes} from "../service/ApiRoutes";
import UserDto from "./user.dto";
import {Button} from "react-native-paper";


export default (props: any) => {


    // 分页数据
    const pageResultUseRequest = useRequest<ApiResult<PageResult<UserDto>>>(
        () => {
            return ApiRoutes.userPage();
        },
        {
            manual: false,
            pollingWhenHidden: false,
            refreshOnWindowFocus: false,
        });

    props.navigation.addListener('focus', () => {
        if (props.route.params?.isGoBack) {
            pageResultUseRequest.run();
        }
    });


    const deleteUseRequest = useRequest<ApiResult<any>>(
        (payload) => {
            return ApiRoutes.userDelete(payload);
        },
        {
            onSuccess: (ar, params) => {
                pageResultUseRequest.run();
            },
            manual: true,
            refreshOnWindowFocus: false,
        },
    );

    return (
        <View
            style={{
                backgroundColor: '#ffffff'
            }}
        >
            <FlatList

                data={pageResultUseRequest?.data?.result?.data}
                renderItem={({item, index, separators}) => <View
                    style={{padding: 20, alignItems: 'center', flexDirection: 'row'}}
                >
                    <View style={{flex: 3}}>
                        <Text>{item.id} - {item.name}</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <Button
                            onPress={() => {
                                Alert.alert(
                                    'Delete Record',
                                    '',
                                    [
                                        {
                                            text: 'Cancel', onPress: () => {
                                            },
                                        },
                                        {
                                            text: 'Ok', onPress: () => {
                                                deleteUseRequest.run({id: item.id});
                                            },
                                        },
                                    ],
                                )

                            }}
                        >
                            Del
                        </Button>
                    </View>
                </View>}
                keyExtractor={(item) => item.id + ''}
            />
        </View>
    )
}