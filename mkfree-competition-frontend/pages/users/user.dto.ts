import BaseDto from "../service/Base.dto";

export default interface UserDto extends BaseDto{
    name?: string;

    checked?:boolean;

    score?:number;
}