import {createStackNavigator} from "@react-navigation/stack";
import React from "react";
import UserIndex from "./UserIndex";
import {View} from "react-native";
import UserAdd from "./UserAdd";
import {Button} from "react-native-paper";

const Users = createStackNavigator();

export default (props: any) => {
    return (
        <Users.Navigator
            screenOptions={({}) => ({
                headerTitle: 'Users',
                headerRight: () => (
                    <Button onPress={() => props.navigation.navigate('UserAdd')}>Add</Button>
                ),
            })}
        >

            <Users.Screen
                name="Users"
                component={UserIndex}
            />

            <Users.Screen
                name="UserAdd"
                component={UserAdd}
                options={{
                    headerTitle: 'UserAdd',
                    headerRight: () => null,

                }}
            />
        </Users.Navigator>
    );
};