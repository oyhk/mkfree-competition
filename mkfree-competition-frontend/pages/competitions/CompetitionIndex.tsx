import {Alert, FlatList, Text, TouchableOpacity, View} from "react-native";
import React, {useState} from "react";
import {useRequest} from "ahooks";
import {ApiResult} from "../service/ApiResult";
import {PageResult} from "../service/PageResult";
import {ApiRoutes} from "../service/ApiRoutes";
import CompetitionDto from "./competition.dto";
import {ActivityIndicator, Button, Colors} from "react-native-paper";
import moment from "moment";

export default (props: any) => {

    const [refreshing, setRefreshing] = useState<boolean>(false);

    // 分页数据
    const pageResultUseRequest = useRequest<ApiResult<PageResult<CompetitionDto>>>(
        (params) => {
            return ApiRoutes.competitionPage(params);
        },
        {
            manual: false,
            pollingWhenHidden: false,
            refreshOnWindowFocus: false,
            onSuccess: (ar, params) => {
                if (params.refreshing) {
                    setRefreshing(false);
                }
            }
        });
    const competitionLogDeleteUseRequest = useRequest<ApiResult<any>>(
        (payload) => {
            return ApiRoutes.competitionDelete(payload);
        },
        {
            onSuccess: (ar, params) => {
                pageResultUseRequest.run();
            },
            manual: true,
            refreshOnWindowFocus: false,
        },
    );

    props.navigation.addListener('focus', () => {
        if (props.route.params?.isGoBack) {
            pageResultUseRequest.run();
        }
    });

    if (!pageResultUseRequest.data) {
        return <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
            <ActivityIndicator animating={true} color={Colors.red800}/>
        </View>
    }


    return (
        <View style={{flex: 1, padding: 10,}}>
            <FlatList
                refreshing={refreshing}
                onRefresh={() => {
                    pageResultUseRequest.run({refreshing: true});
                }}
                data={pageResultUseRequest?.data?.result?.data}
                renderItem={({item, index, separators}) => (
                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                        <TouchableOpacity style={{flex: 3, flexDirection: 'row'}} onPress={() => {
                            props.navigation.push('CompetitionInfo', {id: item.id});
                        }}>
                            <Text>{item.id} - </Text>
                            <Text>{moment(item.createdAt).format('YYYY-MM-DD HH:mm:ss')}</Text>
                        </TouchableOpacity>
                        <View style={{flex: 1}}>
                            <Button
                                mode='text'
                                onPress={() => {
                                    Alert.alert(
                                        'Delete Record',
                                        '',
                                        [
                                            {
                                                text: 'Cancel', onPress: () => {
                                                },
                                            },
                                            {
                                                text: 'Ok', onPress: () => {
                                                    competitionLogDeleteUseRequest.run({id: item.id});
                                                },
                                            },
                                        ],
                                    )

                                }}
                            >
                                Del
                            </Button>
                        </View>
                    </View>
                )}
                keyExtractor={(item) => item.id + ''}
            />
        </View>
    );
}