/**
 *
 */
import UserDto from "../users/user.dto";
import BaseDto from "../service/Base.dto";

export default interface CompetitionLogDto extends BaseDto {
    competitionId: number;
    bomb: number | undefined;
    landlordWin: boolean | undefined;
    landlordUserId: number | undefined;
}