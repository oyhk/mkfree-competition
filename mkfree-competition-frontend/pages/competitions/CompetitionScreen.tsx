import React from "react";
import {createStackNavigator} from "@react-navigation/stack";
import CompetitionIndex from "./CompetitionIndex";
import CompetitionAdd from "./CompetitionAdd";
import CompetitionInfo from "./CompetitionInfo";
import {View} from "react-native";
import {Button} from 'react-native-paper';

const CompetitionStack = createStackNavigator();


export default (props: any) => {
    return (
        <CompetitionStack.Navigator>
            <CompetitionStack.Screen name="Competitions" component={CompetitionIndex}
                                     options={{
                                         headerTitle: 'Competitions',
                                         headerRight: () => (
                                             <Button
                                                 onPress={() => props.navigation.navigate('CompetitionAdd')}>Add</Button>
                                         ),
                                     }}
            />
            <CompetitionStack.Screen name="CompetitionAdd" component={CompetitionAdd}
                                     options={{
                                         headerTitle: 'CompetitionAdd',
                                         headerRight: () => null,
                                     }}
            />
            <CompetitionStack.Screen name="CompetitionInfo" component={CompetitionInfo}
                                     options={{
                                         headerTitle: 'CompetitionInfo',
                                         headerRight: () => null,
                                     }}
            />
        </CompetitionStack.Navigator>
    );
}