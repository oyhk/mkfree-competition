/**
 *
 */
import UserDto from "../users/user.dto";
import BaseDto from "../service/Base.dto";

export default interface CompetitionDto extends BaseDto {
    score: any;
    userList: UserDto [];
    competitionLogList:[];
}