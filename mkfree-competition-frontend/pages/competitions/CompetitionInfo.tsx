import {
    FlatList,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from "react-native";
import React, {useState} from "react";
import {useRequest} from "ahooks";
import {ApiResult} from "../service/ApiResult";
import UserDto from "../users/user.dto";
import {ApiRoutes} from "../service/ApiRoutes";
import CompetitionDto from "./competition.dto";
import CompetitionLogDto from "./competition-log.dto";
import {ActivityIndicator, Button, Colors, RadioButton} from "react-native-paper";
import Icon from 'react-native-vector-icons/FontAwesome5';
import moment from "moment";
import {DataTable} from 'react-native-paper';

export default (props: any) => {

    // 多少炸弹
    const [bombList] = useState([0, 1, 2, 3, 4]);

    const [competitionLog, setCompetitionLog] = useState<CompetitionLogDto>({competitionId: props.route.params.id} as CompetitionLogDto);

    const competitionInfoUseRequest = useRequest<ApiResult<CompetitionDto>>(
        (params) => {
            return ApiRoutes.competitionInfo(params);
        },
        {
            manual: false,
            pollingWhenHidden: false,
            refreshOnWindowFocus: false,
            defaultParams: {id: props.route.params.id}
        });

    const competitionLogSaveUseRequest = useRequest<ApiResult<any>>(
        (payload) => {
            return ApiRoutes.competitionLogSave(payload);
        },
        {
            onSuccess: (ar, params) => {
                if (ar.code === 1) {
                    setCompetitionLog({
                        landlordWin: undefined,
                        landlordUserId: undefined,
                        bomb: undefined,
                        competitionId: props.route.params.id
                    });
                    competitionInfoUseRequest.run({id: props.route.params.id});
                }
            },
            manual: true,
            refreshOnWindowFocus: false,
        },
    );
    const competitionLogDeleteUseRequest = useRequest<ApiResult<any>>(
        (payload) => {
            return ApiRoutes.competitionLogDelete(payload);
        },
        {
            onSuccess: (ar, params) => {
                setCompetitionLog({
                    landlordWin: undefined,
                    landlordUserId: undefined,
                    bomb: undefined,
                    competitionId: props.route.params.id
                });
                competitionInfoUseRequest.run({id: props.route.params.id});
            },
            manual: true,
            refreshOnWindowFocus: false,
        },
    );

    if (!competitionInfoUseRequest.data) {
        return <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
            <ActivityIndicator animating={true} color={Colors.red800}/>
        </View>
    }
    return (
        <View style={{flex: 1}}>
            <View style={{backgroundColor: Colors.indigo100, paddingTop: 10, paddingBottom: 10}}>
                <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginBottom: 10}}>

                    {
                        competitionInfoUseRequest?.data?.result?.userList.map((user: UserDto) => {
                            return <TouchableOpacity
                                style={{flex: 1, alignItems: 'center'}}
                                key={user.id}
                                onPress={() => {
                                    const tempCompetitionLog = {...competitionLog};
                                    tempCompetitionLog.landlordUserId = user.id;
                                    setCompetitionLog(tempCompetitionLog);
                                }}
                            >
                                <Text style={{
                                    fontSize: 30,
                                    paddingBottom: 10,
                                    ...!user.score || user.score === 0 ? {color: 'black'} : user.score > 0 ? {color: 'red'} : user.score < 0 ? {color: 'green'} : {color: 'black'}
                                }}>{user.name}</Text>
                                <Text style={{
                                    fontSize: 30,
                                    paddingBottom: 10,
                                    ...!user.score || user.score === 0 ? {color: 'black'} : user.score > 0 ? {color: 'red'} : user.score < 0 ? {color: 'green'} : {color: 'black'}
                                }}>{user.score}
                                </Text>
                                <RadioButton
                                    value={user.id + ''}
                                    status={user.id === competitionLog.landlordUserId ? 'checked' : 'unchecked'}
                                    onPress={() => {
                                        const tempCompetitionLog = {...competitionLog};
                                        tempCompetitionLog.landlordUserId = user.id;
                                        setCompetitionLog(tempCompetitionLog);
                                    }}
                                />
                            </TouchableOpacity>
                        })
                    }
                </View>

                <View style={{flexDirection: 'row'}}>
                    {
                        bombList.map(bomb => {
                            return <TouchableOpacity
                                style={{flex: 1, alignItems: 'center', padding: 30}}
                                key={'bomb' + bomb}
                                onPress={() => {
                                    const tempCompetitionLog = {...competitionLog};
                                    tempCompetitionLog.bomb = bomb;
                                    setCompetitionLog(tempCompetitionLog);
                                }}
                            >
                                <Text style={{fontSize: 20}}>{bomb}</Text>
                                <RadioButton
                                    value={bomb + ''}
                                    status={bomb === competitionLog.bomb ? 'checked' : 'unchecked'}
                                    onPress={() => {
                                        const tempCompetitionLog = {...competitionLog};
                                        tempCompetitionLog.bomb = bomb;
                                        setCompetitionLog(tempCompetitionLog);
                                    }}
                                />
                            </TouchableOpacity>
                        })
                    }
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                    <View style={{flex: 1, margin: 10}}>
                        <Button color={'red'}
                                mode="contained"
                                onPress={() => {
                                    if (competitionLog.landlordUserId === undefined) {
                                        alert('Landlord must be choose');
                                        return;
                                    }
                                    if (competitionLog.bomb === undefined) {
                                        alert('Bomb must be choose');
                                        return;
                                    }

                                    competitionLog.landlordWin = true;
                                    competitionLogSaveUseRequest.run(competitionLog);
                                }}>Win</Button>
                    </View>
                    <View style={{flex: 1, margin: 10}}>
                        <Button color={'green'}
                                mode="contained"
                                onPress={() => {
                                    if (competitionLog.landlordUserId === undefined) {
                                        alert('Landlord must be choose');
                                        return;
                                    }
                                    if (competitionLog.bomb === undefined) {
                                        alert('Bomb must be choose');
                                        return;
                                    }
                                    competitionLog.landlordWin = false;
                                    competitionLogSaveUseRequest.run(competitionLog);
                                }}>Lose</Button>

                    </View>
                </View>
            </View>
            <View style={{flex: 10, flexDirection: 'row', padding: 10}}>
                <FlatList
                    ItemSeparatorComponent={() => <View style={{height: 1, backgroundColor: '#d9d9d9'}}/>}
                    data={competitionInfoUseRequest?.data?.result?.competitionLogList as { current: string, logList: { userName: string, score: number, createdAt: string, isLandlord: boolean }[] }[]}
                    renderItem={({item, index, separators}) => {
                        return (
                            <View style={{flex: 1, flexDirection: 'row', alignItems: 'center',padding:10}}>
                                <View style={{flex: 1, alignItems: 'flex-start'}}>
                                    <Text>{item.current}</Text>
                                </View>
                                <View style={{
                                    flex: 6,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    flexDirection: 'row'
                                }}>
                                    {
                                        item.logList.map(i =>
                                            <View key={`${item.current}_${i.userName}`}
                                                  style={{flex: 1,alignItems: 'center', justifyContent: 'center'}}>
                                                <Text style={{fontSize: 14}}>{i.score}</Text>
                                                {
                                                    i.isLandlord ? (i.score > 0 ?
                                                        <Icon name="smile" size={14} color="red"/> :
                                                        <Icon name="angry" size={14} color="green"/>) :
                                                        <Text></Text>
                                                }
                                            </View>
                                        )
                                    }
                                </View>
                                <View style={{flex: 3, alignItems: 'flex-end'}}>
                                    <Text>{moment(item?.logList[0]?.createdAt).format('HH:mm:ss')}</Text>
                                </View>
                                <View style={{flex: 2, alignItems: 'flex-end'}}>
                                    <Button onPress={() => {
                                        competitionLogDeleteUseRequest.run({
                                            competitionId: props.route.params.id,
                                            current: item.current
                                        });
                                    }}>Del</Button>
                                </View>
                            </View>
                        );
                    }}
                    keyExtractor={(item) => item.current}
                />
            </View>
        </View>
    );
}