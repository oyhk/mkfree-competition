import {Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, View} from "react-native";
import React, {useState} from "react";
import {useRequest} from "ahooks";
import {ApiResult} from "../service/ApiResult";
import UserDto from "../users/user.dto";
import {ApiRoutes} from "../service/ApiRoutes";
import CompetitionDto from "./competition.dto";
import {ActivityIndicator, Button, Checkbox, Colors} from 'react-native-paper';

export default (props: any) => {

    const [competition, setCompetition] = useState<CompetitionDto>({} as CompetitionDto);
    const [userList, setUserList] = useState<UserDto[]>([]);

    // 分页数据
    const userListUseRequest = useRequest<ApiResult<UserDto[]>>(
        () => {
            return ApiRoutes.userList();
        },
        {
            manual: false,
            pollingWhenHidden: false,
            refreshOnWindowFocus: false,
            onSuccess: (ar) => {
                const userList = ar?.result?.map((user: UserDto) => ({
                    id: user.id,
                    name: user.name,
                    checked: false
                }));
                if (userList) {
                    setUserList(userList);
                }
            }
        });

    const projectSaveUseRequest = useRequest<ApiResult<any>>(
        (payload) => {
            return ApiRoutes.competitionSave(payload);
        },
        {
            onSuccess: (ar, params) => {
                if (ar.code === 1) {
                    props.navigation.replace('CompetitionInfo', {id: ar?.result?.id});
                } else {
                }
            },
            manual: true,
            refreshOnWindowFocus: false,
        },
    );

    if (!userListUseRequest.data) {
        return <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
            <ActivityIndicator animating={true} color={Colors.red800}/>
        </View>
    }


    return (
        <View style={{flex: 1}}>
            {
                userList.length < 3 ?
                    <View style={{padding: 10}}><Text style={{color: 'red'}}>No less than 3 people</Text></View> : <View></View>
            }
            {
                userList?.map(user =>
                    <TouchableOpacity key={user.id}
                                      style={{flexDirection: 'row', alignItems: 'center',padding:10}}
                                      onPress={() => {
                                          const tempUserCheckboxList = [...userList];
                                          tempUserCheckboxList.forEach(tempUser => {
                                              if (tempUser.id === user.id) {
                                                  tempUser.checked = !tempUser.checked;
                                              }
                                          });
                                          setUserList(tempUserCheckboxList);
                                      }}
                    >
                        <Checkbox
                            status={user.checked ? 'checked' : 'unchecked'}
                            onPress={() => {
                                const tempUserCheckboxList = [...userList];
                                tempUserCheckboxList.forEach(tempUser => {
                                    if (tempUser.id === user.id) {
                                        tempUser.checked = !tempUser.checked;
                                    }
                                });
                                setUserList(tempUserCheckboxList);
                            }}
                        />
                        <Text>{user.name}</Text>
                    </TouchableOpacity>
                )
            }

            <View style={{flexDirection: 'row', padding: 10}}>
                <Text style={{flex: 1, height: 40, lineHeight: 40}}>Score：</Text>
                <TextInput keyboardType={'numeric'} onChangeText={(value) => competition.score = parseInt(value)}
                           style={{flex: 3, height: 40, borderColor: 'gray', borderWidth: 1}}/>
            </View>

            <View style={{margin: 20}}>
                <Button
                    mode={'contained'}
                    onPress={() => {
                        const checkUserList = userList?.filter(value => value.checked);
                        if (checkUserList.length !== 3) {
                            alert('Please choose 3 players');
                            return;
                        }
                        if (typeof competition.score !== 'number') {
                            alert('Score must be a number');
                            return;
                        }
                        checkUserList.forEach((value) => {
                            value.checked = undefined;
                        });
                        competition.userList = checkUserList;
                        projectSaveUseRequest.run(competition);
                    }}
                >Submit</Button>
            </View>
        </View>
    );
}