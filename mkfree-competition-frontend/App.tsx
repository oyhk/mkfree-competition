import React from 'react';
import {UseRequestProvider} from 'ahooks';
import {NavigationContainer} from '@react-navigation/native';
import UserScreen from "./pages/users/UserScreen";
import CompetitionScreen from "./pages/competitions/CompetitionScreen";
import {createDrawerNavigator} from '@react-navigation/drawer';

const Drawer = createDrawerNavigator();


export default () => {

    return (
        <UseRequestProvider
            value={{}}
        >
            <NavigationContainer>
                <Drawer.Navigator>
                    <Drawer.Screen name="Competitions" component={CompetitionScreen}/>
                    <Drawer.Screen name="Users" component={UserScreen}/>
                </Drawer.Navigator>
            </NavigationContainer>
        </UseRequestProvider>
    );
}
